int main(int argc, int * argv[]) {

	/*
 	* OpenMPI Cluster Architecture
 	*
 	* OpenMPI is a Master/Slave structure, with processor number 0 as the process cordinator, sending all the works to process number which are not 0 (known as slave/worker).
 	*
 	* End of OpenMPI Cluster Architecture
 	*/

	/*		
	** Library Routine Introduction
	*
	* MPI_Init - MPI initialization, will return error code for debug purpose, else is success initialization
	* MPI_Send - Send layer 7 datapacket to the processes in the cluster	
	* MPI_Recv - Receive datapacket from process coordinator ( processor coordinator is a processor number with 0
 	*
 	** End of Introduction
  	*/
  
	/* Declarations */
	int i, numberProcessors, myProcessorNumber, sum, result;
	int intArray[100];
	int myChunk[25];
	int err; 
	MPI_Status status;

	/* Initialize MPI. */
	err = MPI_Init( & argc, & argv);
	err = MPI_Comm_size(MPI_COMM_WORLD, & numberProcessors); //comm world is a communicator responsible for communication between processes
	err = MPI_Comm_rank(MPI_COMM_WORLD, & myProcessorNumber); //rank in the group, a group is a ordered set of process, each given a rank
  
  
	/* Take two different actions depending on whether this is the main processor or not.*/
	if (myProcessorNumber == 0) {

		/* Array initialization */
		initArray(intArray, 100);

		/* I am the main processor, so I distribute the problem to processors. */
		for (i = 1; i < numberProcessors; i++) {

			/* Send chunks of array out to each processor. Arguments are: pointer to starting address, number of items sent, type of items sent, destination processor, message id, group of processors which are eligible to receive the message (in the case of MPI_COMM_WORLD, all of them) */
			err = MPI_Send( & intArray[i * 25], 25, MPI_INT, i, 100, MPI_COMM_WORLD);

			/* Copy main processor's data into its chunk array. Assume function copyArray copies n integers from one integer array to another integer array. */
			copyArray(intArray, myChunk, 25);
		}
	} else {

		/* I am not the main processor, so I receive a chunk to work on. Arguments are: buffer in which to receive data, number of items sent, type of items sent, destination processor, message id, group of processors which are eligible to receive the message, pointer to a status variable (contains information about status of transmission) */ 
		err = MPI_Recv( & myChunk[0], 25, MPI_INT, 0, 100, MPI_COMM_WORLD, & status);
	} // End of distributing work for coordinato or receiving work for workers/slave


	/* Now that the problem is distributed, solve it. Each processor will have its share of the work in the myChunk array. */
	sum = 0;
	for (i = 0; i < 25; i++) {
		sum = sum + myChunk[i];
	}

	/* Print out the sums each processor calculates. */
	printf("%d summed %d\n", myProcessorNumber, sum);
	/* Send the results back to the main processor. */
	if (myProcessorNumber == 0) {

		/* I receive the partial results and compute the total result. */
		for (i = 1; i < numberProcessors; i++) {
			err = MPI_Recv( & result, 1, MPI_INT, i, 200, MPI_COMM_WORLD, & status);
			sum = sum + result;
		}
	} else {

		/* I am not the main processor, so I send off my results. */
		err = MPI_Send( & sum, 1, MPI_INT, 0, 200, MPI_COMM_WORLD);
	}
	/* Do something with the final result. */
	/* Have MPI perform its shut-down. */
	err = MPI_Finalize();
}
